package com.kayhut.fuse.assembly.knowledge.domain;

public abstract class EntityId extends Metadata{
    public String entityId;
    public String logicalId;
    public String relationId;
}
