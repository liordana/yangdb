package com.kayhut.fuse.assembly.knowledge.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        KnowledgeIdGenSnowflakeTests.class
})
public class KnowledgeIdGenTestSuite {
}
